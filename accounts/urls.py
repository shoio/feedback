from django.urls import path
from .views import AddUserCreateView


app_name = 'accounts'

urlpatterns = [
	path('', AddUserCreateView.as_view(), name='add_user'),
]