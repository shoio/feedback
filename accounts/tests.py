import tempfile
from django.test import TestCase
from django.contrib.auth.models import User
from .models import Profile


class ProfileTestCase(TestCase):
	def setUp(self):
		joao = User.objects.create(first_name="João", username='joao')
		gabriel = User.objects.create(first_name="Gabriel", username='gabriel')
		Profile.objects.create(
			user=joao,
			photo=tempfile.NamedTemporaryFile(suffix='.jpg').name,
			bio="Mini bio",
			birthday="1984-07-18",
			genre="M",
			cell_phone="(48)98870-0178",
		)
		Profile.objects.create(
			user=gabriel,
			photo=tempfile.NamedTemporaryFile(suffix='.jpg').name,
			bio="Mini bio--------",
			birthday="1954-07-25",
			genre="M",
			cell_phone="(48)98800-0000",
		)

	def test_create_new_profile(self):
		p1 = Profile.objects.get(user__username='joao')
		p2 = Profile.objects.get(user__username='gabriel')
		self.assertEqual(p1.__str__(), "João")
		self.assertEqual(p2.__str__(), "Gabriel")