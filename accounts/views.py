from django.views.generic import CreateView
from django.contrib.auth.models import User


class AddUserCreateView(CreateView):
	model = User
	template_name = 'accounts/add_user.html'
	fields = [
		'first_name',
		'last_name',
		'username',
		'email',
		'password'
	]