from django.test import TestCase
from django.contrib.auth.models import User
from .models import Circle, Comment


class CircleTestCase(TestCase):
	Circle.objects.create(name="Família")
	Circle.objects.create(name="Amigos")

	def test_create_new_circle(self):
		family = Circle.objects.get(name="Família")
		friends = Circle.objects.get(name="Amigos")
		self.assertEqual(family.__str__(), 'Família')
		self.assertEqual(friends.__str__(), 'Ámigos')